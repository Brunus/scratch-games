# Incantation est basé sur le gameplay de Space Invaders

## Principe du jeu
On m'a proposé de coder un Space Invader et ça m'a bien branché, mais en m'informant également que certaines filles des classes avec option algo/code trouvaient qu'on ne leur présentait que des jeux pour garçons...Pac Man un jeu pour garçon ?

Bref ! Du coup j'ai "girlysé" Space Invaders : Des chauves souris ensorcelées, contrôlées par un sorcier, attaquent le village des fées.
La chef des fées doit défendre le village en lançant des sorts aux chauve-souris qui si elles sont touchées, son transformées en papillons et sont libérées du sortilège.

![Alt text](screenshot_Incantation.png)  

## Notions d'algo acquisent avec ce jeu
- Boucles simples et conditionnelles
- Conditions simple et imbriquées
- Variables
- Variables d'état (1 ou 0)
- Envoie et réception de message
- Détermination et utilisation d'une valeur aléatoire
- Temporisation

Lorsqu'on utilise une version 2.x de Scratch, on peut remplacer certaines suites de blocs par des "blocs personnalisés", et du coup introduire la notion de fonction, et c'est très pratique pour régler certains paramètres du jeu comme la rapidité de déplacement des objets.

## Notes sur le graphisme
J'ai utilisé des sprites soit créés par moi même, soit provenant de la collection Scratch et pour la fée provenant des graphismes du jeu Battle for Wesnoth.

Pour le fond d'écran c'est un assemblage (avec traitement couleurs) de graphismes créés par Susann et sous licence CC : https://pixabay.com/en/users/susannp4-1777190/?tab=latest
