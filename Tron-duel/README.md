# Une version du duel de motos-lumière de Tron[1], le film des années 80
Le principe est que les vaisseaux ou motos tracent derrière eux des traits de lumière.  
Un joueur gagne le round en enfermant l'adversaire et en le faisant s'écraser sur un trait de couleur.    

![Alt text](screenshot_TronDuel.png)

[1] https://fr.wikipedia.org/wiki/Tron

## Notions d'algo acquises avec ce jeu
- Boucles
- Conditions
- Variables
- Envoi et réception de message
- Temporisation
