# Breakout est un clone du jeu Breakout d'Atari, avec des idées venues de sa version évoluée Arkanoïd

Version Alpha : ce jeu est en cours de réalisation

## Principe du jeu
Avec le paddle, dirigé à la souris, on renvoit une balle contre le mur de briques.
L'impact de la balle fait disparaître les briques colorés mais n'affecte pas les briques grises.
Certaines brique, aléatoirement, relachent un bonus qu'il faut attraper avec le paddle pour en bénéficier.

![Breakout Screenshot](screenshot_Breakout.png) 

## Notions d'algo acquisent avec ce jeu
- Utilisation des directions
- Boucles simples et conditionnelles
- Conditions simple et imbriquées
- Variables
- Variables d'état (1 ou 0)
- Envoie et réception de message
- Détermination et utilisation d'une valeur aléatoire
- Temporisation

Lorsqu'on utilise une version 2.x de Scratch, on peut remplacer certaines suites de blocs par des "blocs personnalisés", et du coup introduire la notion de fonction, et c'est très pratique pour régler certains paramètres du jeu comme la rapidité de déplacement des objets.
